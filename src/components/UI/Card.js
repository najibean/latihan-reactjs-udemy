import React from 'react'
import './Card.css'

/** untuk build wrapper yang bisa reusable */
function Card(props) {
    /** menggabungkan dengan class name pada parent (props nya) */
    const classes = 'card ' + props.className

    /** 
     * "children" is a reserved name, it doesn't declare in props of user Card 
     * props.children adalah isi didalam tag <Card> dan </Card>
    */
    return <div className={classes}>{props.children}</div>
}

export default Card