- remove button, karena hanya dipakai untuk demo diawal course.
- remove fungsi handler dari button tersebut.
- remove useState.

<br>
<br>

```
/** 
* useState() hanya bisa dipanggil dari React function utama dalam component.
* [title, setTitle] ==> title adalah pointer dari useState(props.title), setTitle adalah untuk menge-set value baru dari variabel title.
* sama dengan setter dan getter pada java.
* props pada JSX pun diubah sama dengan state.
*/

/** 
 * onClick pada attribute adalah sebuah event.
 * pada function clickHandler, jika ditulis {clickHandler()} maka akan langsung dieksekusi saat load page.
 * sehingga hanya diketik dengan {clickHandler} saja, dan hanya akan tereksekusi ketika element di click!
 * 
*/
```