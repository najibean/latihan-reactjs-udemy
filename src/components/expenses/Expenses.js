import React, { useState } from "react"
import "./Expenses.css"
import Card from "../UI/Card"
import ExpensesFilter from "./ExpensesFilter"
import ExpensesList from "./ExpensesList"

function Expenses(props) {
	// const satu = props.dataExpenses[0]
	// const dua = props.dataExpenses[1]
	// const tiga = props.dataExpenses[2]
	// const empat = props.dataExpenses[3]

	/* angka tahun didalam useState() adalah untuk default value yang pertama kali muncul jika reload browser */
	const [filteredYear, setFilteredYear] = useState("2021")

	const filterChangeHandler = selectedYear => {
		setFilteredYear(selectedYear)
	}

	const filteredExpenses = props.dataExpenses.filter(expense => {
		return expense.date.getFullYear().toString() === filteredYear
	})

	return (
		<li>
			<div>
				<Card className="expenses">
					<ExpensesFilter
						selected={filteredYear}
						onChangeFilter={filterChangeHandler}
					/>
					<ExpensesList items={filteredExpenses} />
				</Card>
			</div>
		</li>
	)
}

export default Expenses

/* memasukkan array kedalam map() dengan hasil render-an yang sama */
/* untuk yang di map, harus dimasukkan value yang unique berupa key atau id */
/* {filteredExpenses.map(expense => (
  <ExpenseItem
    key={expense.id}
    title={expense.title}
    amount={expense.amount}
    date={expense.date}
  />
))} */
