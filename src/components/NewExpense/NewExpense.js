import React, { useState } from "react"
import "./NewExpense.css"
import ExpenseForm from "./ExpenseForm"

const NewExpense = props => {
	const [isEditing, setIsEditing] = useState(false)
	const saveExpenseDataHandler = dataExpenses => {
		const expenseData = {
			...dataExpenses,
			id: Math.random().toString()
		}
		props.onAddExpense(expenseData)
		setIsEditing(false)
	}

	const startEditingHandler = () => {
		setIsEditing(true)
	}

	const stopEditingHandler = () => {
		setIsEditing(false)
	}

	return (
		<div className="new-expense">
			{/* Jika isEditing FALSE maka dieksekusi yang button, jika TRUE maka dieksekusi yang ExpenseForm  */}
			{!isEditing && (
				<button onClick={startEditingHandler}>Add New Expense</button>
			)}
			{isEditing && (
				<ExpenseForm
					onSaveExpenseData={saveExpenseDataHandler}
					onCancel={stopEditingHandler}
				/>
			)}
		</div>
	)
}

export default NewExpense
