- alternative dari useState nya, jika useState digabungkan
```
/* [ALTERNATIVE] */
const [userInput, setUserInput] = useState({
    enteredTitle: '',
    enteredAmount: '',
    enteredDate: ''
})
```

- alternative dari fungsi handler nya
```
/* [ALTERNATIVE] */
const titleChangeHandler = (event) => {
   setUserInput({
       ...userInput,
       enteredTitle: event.target.value
   })

    /* agar state selalu mangambil dari yang ter-update, JANGAN gunakan yang diatas*/
    setUserInput((prevState) => {
        return { ...prevState, enteredTitle: event.target.value }
    })
}
const amountChangeHandler = (event) => {
   setUserInput({
       ...userInput,
       enteredAmount: event.target.value
   })

    /* agar state selalu mangambil dari yang ter-update, JANGAN gunakan yang diatas*/
    setUserInput((prevState) => {
        return { ...prevState, enteredAmount: event.target.value }
    })
}
const dateChangeHandler = (event) => {
   setUserInput({
       ...userInput,
       enteredDate: event.target.value
   })

    /* agar state selalu mangambil dari yang ter-update, JANGAN gunakan yang diatas*/
    setUserInput((prevState) => {
        return { ...prevState, enteredDate: event.target.value }
    })
}
```