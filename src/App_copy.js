import React from 'react'
import Expenses from './components/expenses/Expenses'

function App() {
  /** 
   * jika dilakukan dengan regular javascript seperti dibawah ini, maka disebut juga dengan imperative approach 
   * imperative maksudnya adalah memberikan instruksi yang jelas, step by step yang harus dilakukan oleh javascript
  */
  // const paragraf = document.createElement('p');
  // paragraf.textContent = 'Ini juga visible';
  // document.getElementById('root').append(paragraf);
  
  /** data yang dimasukkan ke props */
  const expenses = [
    {
      id: 'e1',
      title: 'Toilet Paper',
      amount: 94.12,
      date: new Date(2020, 7, 14),
    },
    { id: 'e2', title: 'New TV', amount: 799.49, date: new Date(2021, 2, 12) },
    {
      id: 'e3',
      title: 'Car Insurance',
      amount: 294.67,
      date: new Date(2021, 2, 28),
    },
    {
      id: 'e4',
      title: 'New Desk (Wooden)',
      amount: 450,
      date: new Date(2021, 5, 12),
    }
  ]

  /** penulisan jika TIDAK menggunakan JSX */
  // return React.createElement(
  //   'div',
  //   {},
  //   React.createElement('h2', {}, "Let's get started"),
  //   React.createElement(Expenses, { dataExpenses: expenses })
  // )

  return (
    <div>
      <h2>Let's get started Najib</h2>
      <Expenses dataExpenses={expenses}/>
    </div>
  );
}

export default App;

/** JSX stands for JavaScript XML */